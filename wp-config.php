<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bnet' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9<:xAc$jr_fMlRmMJl^qK?R}il69n;/HvBM>0B~uxzs%;tl<BVv,2^#0jFceGa])' );
define( 'SECURE_AUTH_KEY',  'to2DIV/L7xti8q@&!,bt[_pnB=1CcUf;j#jAL{AMu*|iar~P#jv*%ewWC.@ssFdT' );
define( 'LOGGED_IN_KEY',    'VuE83uQ[~/cA|;wjO>Q]mOswj9:gY`1,]RB]cXs a.y>cGJj/z^Wc@k&SO`bU<M*' );
define( 'NONCE_KEY',        'i[u}!VocoZehLd-fRl^J?DK9UqP]dg1V3}~H=S,K1-L0/4~jxw%)Q/q|bss7~5H3' );
define( 'AUTH_SALT',        'Vm`$)%3vYn5PXo6{@Q:/Kv5@|p?}s>4C%sQIW>0d.5IR]:3r7l8b;/rvAbcgQJit' );
define( 'SECURE_AUTH_SALT', '`V#m-H2Y_*G`YJpl_$(3lP8WMtT&743IB-@2-=:&~8&@q}sAPytA*.cpgT,x`{Dp' );
define( 'LOGGED_IN_SALT',   '@Fw5=a_+()!&^ri(&5>6IxSa>,/R=tJTw5/`QV&ovae(y@@w=tMn~n`y@JI(e?fK' );
define( 'NONCE_SALT',       ' .Ks?.i=?SMBo<G tXHy;Fz@MV6@Q(OKR^w/,/43{:TYj(lvN?wsb )qu`hof3w ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

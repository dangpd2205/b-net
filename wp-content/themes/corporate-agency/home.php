<?php
/*
 Template Name: Home
 */
?>

<!doctype html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>C-Agency</title>
    <meta name='robots' content='noindex,follow' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="C-Agency &raquo; Feed" href="wp-content/themes/corporate-agency/assets/xml/feed.xml" />
    <link rel="alternate" type="application/rss+xml" title="C-Agency &raquo; Comments Feed" href="wp-content/themes/corporate-agency/assets/xml/feed_comments.xml/" />
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "https:\/\/themewidget.com\/corporateagency.demo\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.13"
            }
        };
        ! function(e, a, t) {
            var n, r, o, i = a.createElement("canvas"),
                p = i.getContext && i.getContext("2d");

            function s(e, t) {
                var a = String.fromCharCode;
                p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0);
                e = i.toDataURL();
                return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
            }

            function c(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }
            for (o = Array("flag", "emoji"), t.supports = {
                    everything: !0,
                    everythingExceptFlag: !0
                }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
                if (!p || !p.fillText) return !1;
                switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
                    case "flag":
                        return s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) ? !1 : !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
                    case "emoji":
                        return !s([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039])
                }
                return !1
            }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
                t.DOMReady = !0
            }, t.supports.everything || (n = function() {
                t.readyCallback()
            }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
                "complete" === a.readyState && t.readyCallback()
            })), (n = t.source || {}).concatemoji ? c(n.concatemoji) : n.wpemoji && n.twemoji && (c(n.twemoji), c(n.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css' href='wp-content/themes/corporate-agency/assets/css/customizer/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css' href='wp-content/themes/corporate-agency/assets/css/customizer/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css' href='wp-content/themes/corporate-agency/assets/css/customizer/woocommerce-layout.css' type='text/css' media='all' />
    <style id='woocommerce-layout-inline-css' type='text/css'>
        .infinite-scroll .woocommerce-pagination {
            display: none;
        }
    </style>
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='wp-content/themes/corporate-agency/assets/css/customizer/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href='wp-content/themes/corporate-agency/assets/css/customizer/woocommerce.css' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <link rel='stylesheet' id='corporate-agency-style-css' href='wp-content/themes/corporate-agency/assets/css/customizer/style2.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='corporate-agency-fonts-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300i%2C400%2C400i%2C600%2C600i%2C700%2C700i%2C800%2C800i&#038;subset=latin%2Clatin-ext&#038;ver=5.0.13' type='text/css' media='all' />
    <link rel='stylesheet' id='animate-css' href='wp-content/themes/corporate-agency/assets/css/customizer/animate.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css' href='wp-content/themes/corporate-agency/assets/css/customizer/bootstrap.css' type='text/css' media='all' />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel='stylesheet' id='magnific-popup-css' href='wp-content/themes/corporate-agency/assets/css/customizer/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-carousel-css' href='wp-content/themes/corporate-agency/assets/css/customizer/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' id='corporate-agency-main-css' href='wp-content/themes/corporate-agency/assets/css/customizer/main.css' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href='wp-content/themes/corporate-agency/assets/css/customizer/js_composer.min.css' type='text/css' media='all' />
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/jquery.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/jquery.blockUI.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/corporateagency.demo\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/corporateagency.demo\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/themewidget.com\/corporateagency.demo\/index.php\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://themewidget.com/corporateagency.demo/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.5.3'></script>
    <script type='text/javascript' src='https://themewidget.com/corporateagency.demo/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=4.12.1'></script>
    <link rel='https://api.w.org/' href='https://themewidget.com/corporateagency.demo/index.php/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://themewidget.com/corporateagency.demo/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://themewidget.com/corporateagency.demo/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.0.13" />
    <meta name="generator" content="WooCommerce 3.5.3" />
    <link rel="canonical" href="https://themewidget.com/corporateagency.demo/" />
    <link rel='shortlink' href='https://themewidget.com/corporateagency.demo/' />
    <link rel="alternate" type="application/json+oembed" href="https://themewidget.com/corporateagency.demo/index.php/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemewidget.com%2Fcorporateagency.demo%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://themewidget.com/corporateagency.demo/index.php/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemewidget.com%2Fcorporateagency.demo%2F&#038;format=xml" />
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <style type="text/css">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress." />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://themewidget.com/corporateagency.demo/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="https://themewidget.com/corporateagency.demo/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]-->
    <style type="text/css">
        .site-title,
        .site-description {
            position: absolute;
            clip: rect(1px, 1px, 1px, 1px);
        }
    </style>
    <style type="text/css" id="wp-custom-css">
        #section5 #section5.section-5-background {
            padding: 0 !important;
            background-color: transparent !important;
        }

        .section-5-background+.section-8 {
            padding: 0;
            margin: 0;
        }
    </style>
    <noscript>
        <style type="text/css"> 
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>
</head>

<body class="home page-template page-template-template-home page-template-template-home-php page page-id-113 wp-custom-logo woocommerce-no-js wpb-js-composer js-comp-ver-4.12.1 vc_responsive">
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

        <header id="header" class="head">
            <div class="top-header">
                <div class="container">
                    <div class="row ">
                        <ul class="contact-detail2 col-sm-6 pull-left">
                            <li><a href="#" target="_blank"><i class="fa fa-mobile"></i>Call VN + 84 865 615 197</a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-envelope-o"></i>hello@domain.com</a></li>
                        </ul>
                        <div class="social-links col-sm-6 pull-right">
                            <ul class="social-icons pull-right">
                                <li>
                                    <a href="https://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com" target="_blank"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="https://pinterest.com" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default navbar-1">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="https://themewidget.com/corporateagency.demo/" class="custom-logo-link" rel="home" itemprop="url"><img width="200" height="71" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/logo-2.png" class="custom-logo" alt="C-Agency" itemprop="logo" /></a>
                    </div>
                    <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                        <ul id="menu-menu-1" class="nav navbar-nav navbar-right">
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-72" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-72 active"><a title="Home" href="https://themewidget.com/corporateagency.demo/">Home</a></li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74"><a title="About us" href="https://themewidget.com/corporateagency.demo/index.php/about/">About us</a></li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87"><a title="Services" href="https://themewidget.com/corporateagency.demo/index.php/services/">Services</a></li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-307" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-307"><a title="Blog" href="https://themewidget.com/corporateagency.demo/index.php/category/blog/">Blog</a></li>
                            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a title="Contact" href="https://themewidget.com/corporateagency.demo/index.php/contact/">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <section id="slider" class="">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/office-620822_1920-1200x600.jpg" class="img-responsive" alt="Lorem ipsum dolor at">
                        <div class="carousel-caption">
                            <h1 class="wow slideInLeft color-white">Lorem ipsum dolor at</h1>
                            <h3 class="wow slideInRight color-white">
                                <p>Excepteur sint occaecat cupidatat non proident </p>
                            </h3>
                            <a href="https://themewidget.com/corporateagency.demo/index.php/lorem-ipsum-dolor-sit-amet/" class="btn btn-seconday wow bounceInUp">Know More<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="item ">
                        <img src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/action-2277292_1920-1200x600.jpg" class="img-responsive" alt="Sed ut perspiciatis omnis">
                        <div class="carousel-caption">
                            <h1 class="wow slideInLeft color-white">Sed ut perspiciatis omnis</h1>
                            <h3 class="wow slideInRight color-white">
                                <p>Nemo enim ipsam voluptatem quia</p>
                            </h3>
                            <a href="https://themewidget.com/corporateagency.demo/index.php/sed-ut-perspiciatis-omnis/" class="btn btn-seconday wow bounceInUp">Know More<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- Carousel controls -->

                <a class="carousel-control left" href="#myCarousel" data-slide="prev"> <span class="carousel-arrow"> <i class="fa fa-angle-left fa-2x"></i></span> </a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next"> <span class="carousel-arrow"><i class="fa fa-angle-right fa-2x"></i></span> </a>
            </div>
        </section>
        <section id="section1" class="section-margine">
            <div class="container">
                <div class="row">
                    <div class="sec-title text-center">
                        <span class="tagline">We help to Grow Your Business</span>
                        <h2>WELCOME TO CONSULTANT BUSINESS</h2>
                        <span class="double-border"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 ">
                        <div class="section-1-box wow bounceIn">
                            <div class="section-1-box-icon-background">
                                <img width="512" height="512" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/computer-1.png" class="img-responsive wp-post-image" alt="Adipiscing elit" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/computer-1.png 512w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/computer-1-150x150.png 150w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/computer-1-300x300.png 300w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/computer-1-450x450.png 450w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/computer-1-100x100.png 100w" sizes="(max-width: 512px) 100vw, 512px" />
                            </div>
                            <h4 class="text-center">Adipiscing elit</h4>
                            <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 ">
                        <div class="section-1-box wow bounceIn">
                            <div class="section-1-box-icon-background">
                                <img width="512" height="512" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/process-1.png" class="img-responsive wp-post-image" alt="Duis aute irure" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/process-1.png 512w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/process-1-150x150.png 150w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/process-1-300x300.png 300w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/process-1-450x450.png 450w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/process-1-100x100.png 100w" sizes="(max-width: 512px) 100vw, 512px" />
                            </div>
                            <h4 class="text-center">Duis aute irure</h4>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 ">
                        <div class="section-1-box wow bounceIn">
                            <div class="section-1-box-icon-background">
                                <img width="512" height="512" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/real-estate-1.png" class="img-responsive wp-post-image" alt="Nemo enim ipsam" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/real-estate-1.png 512w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/real-estate-1-150x150.png 150w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/real-estate-1-300x300.png 300w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/real-estate-1-450x450.png 450w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/real-estate-1-100x100.png 100w" sizes="(max-width: 512px) 100vw, 512px" />
                            </div>
                            <h4 class="text-center">Nemo enim ipsam</h4>
                            <p>Adipisci velit, sed quia non numquam eius modi tempora incidunt</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section4" class="section-margine">
            <div class="container">
                <div class="row">
                    <div class="sec-title text-center">
                        <span class="tagline">We help to Grow Your Business</span>
                        <h2>OUR PROJECTS</h2>
                        <span class="double-border"></span>
                    </div>
                    <div class="col-md-12 col-md-12">
                        <div class="portfolioFilter text-center">
                            <a href="#" data-filter="*" class="current">All</a>
                            <a href="#" data-filter=".consulting">consulting</a>
                            <a href="#" data-filter=".finance">finance</a>
                            <a href="#" data-filter=".growth">growth</a>
                        </div>

                        <div class="portfolioContainer">
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center growth">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio3" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center growth">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio3" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center finance">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center growth">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio7" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center finance">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6-1.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6-1.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio6" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6-1.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6-1-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center finance">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5-1.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5-1.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio4" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5-1.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/5-1-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center consulting">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1.jpg" class="img-responsive wow zoomIn wp-post-image" alt="portfolio3" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center consulting">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8.jpg" class="img-responsive wow zoomIn wp-post-image" alt="2" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center consulting">
                                <a class="magnific-popup" href="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/9.jpg">
                                    <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/9.jpg" class="img-responsive wow zoomIn wp-post-image" alt="1" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/9.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/9-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" /> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section5" class="section-margine section-5-background" style="background-image: url(https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/section-5-background.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="section-5-box-text-cont wow fadeInUp">
                            <h2>GROW UP YOUR BUSINESS WITH US</h2>
                            <section id="section5" class="section-margine section-5-background">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12">
                                            <div class="section-5-box-text-cont wow fadeInUp">
                                                <p>We alawys try to provide you our best business consulting service</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section id="section8" class="section-8 section-margine">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section8" class="section-8 section-margine" style="margin-top: 70px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="sec-title text-center wow fadeInUp">
                            <span class="tagline">Happy Clients</span>
                            <h2>WHAT&#039;S OUR CLIENT&#039;S SAYS</h2>
                            <span class="double-border"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="testimonial_wrapper">
                            <div class="testimonial_slider wow fadeInUp">
                                <div class="single_testimonial">
                                    <div class="testmonial_img">
                                        <img width="450" height="450" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/2-3.jpg" class="attachment-corporate-agency-thubmnail-2 size-corporate-agency-thubmnail-2 wp-post-image" alt="Don Flethcer" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/2-3.jpg 450w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/2-3-150x150.jpg 150w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/2-3-300x300.jpg 300w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/2-3-100x100.jpg 100w" sizes="(max-width: 450px) 100vw, 450px" />
                                    </div>
                                    <div class="testimonial_contents">
                                        <div class="name_desig">
                                            <p class="name">Don Flethcer</p>
                                        </div>
                                        <div class="testimonial_text">
                                            <p>Nam liber tempor cum soluta nobis eleifend option congue mperdiet doming id quod mazim placerat facer possim as. Typi non habent claritatem insitam.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_testimonial">
                                    <div class="testmonial_img">
                                        <img width="450" height="450" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3-450x450.jpg" class="attachment-corporate-agency-thubmnail-2 size-corporate-agency-thubmnail-2 wp-post-image" alt="Mack raider" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3-450x450.jpg 450w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3-150x150.jpg 150w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3-300x300.jpg 300w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3-600x600.jpg 600w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3-100x100.jpg 100w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1-3.jpg 755w" sizes="(max-width: 450px) 100vw, 450px" />
                                    </div>
                                    <div class="testimonial_contents">
                                        <div class="name_desig">
                                            <p class="name">Mack raider</p>
                                        </div>
                                        <div class="testimonial_text">
                                            <p>Nam liber tempor cum soluta nobis eleifend option congue mperdiet doming id quod mazim placerat facer possim as. Typi non habent claritatem insitam.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section14" class="section-margine">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="sec-title text-center">
                            <span class="tagline">The latest headlines</span>
                            <h2>CONSULTING EXPERTS NEWS</h2>
                            <span class="double-border"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="section-14-box wow fadeInUp">
                            <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/action-2277292_1920-370x240.jpg" class="img-responsive wp-post-image" alt="Financial Projections" />
                            <h3><a href="https://themewidget.com/corporateagency.demo/index.php/2018/12/25/financial-projections/">Financial Projections</a></h3>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="text-left comments">
                                        <i class="fa fa-user"></i>
                                        <a href="https://themewidget.com/corporateagency.demo/index.php/author/admin/">
                                            admin </a>
                                    </div>
                                    <div class="date">
                                        <span><i class="fa fa-calendar"></i> December 25, 2018</span>
                                    </div>
                                </div>
                            </div>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when [&hellip;]</p>
                            <a href="https://themewidget.com/corporateagency.demo/index.php/2018/12/25/financial-projections/" class="btn btn-primary">Read More <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="section-14-box wow fadeInUp">
                            <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8-1.jpg" class="img-responsive wp-post-image" alt="Audit &#038; Assurance" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8-1.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/8-1-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" />
                            <h3><a href="https://themewidget.com/corporateagency.demo/index.php/2018/12/25/audit-assurance/">Audit &#038; Assurance</a></h3>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="text-left comments">
                                        <i class="fa fa-user"></i>
                                        <a href="https://themewidget.com/corporateagency.demo/index.php/author/admin/">
                                            admin </a>
                                    </div>
                                    <div class="date">
                                        <span><i class="fa fa-calendar"></i> December 25, 2018</span>
                                    </div>
                                </div>
                            </div>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when [&hellip;]</p>
                            <a href="https://themewidget.com/corporateagency.demo/index.php/2018/12/25/audit-assurance/" class="btn btn-primary">Read More <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="section-14-box wow fadeInUp">
                            <img width="370" height="240" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1.jpg" class="img-responsive wp-post-image" alt="Investment Planning" srcset="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1.jpg 370w, https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/7-1-300x195.jpg 300w" sizes="(max-width: 370px) 100vw, 370px" />
                            <h3><a href="https://themewidget.com/corporateagency.demo/index.php/2018/12/25/investment-planning/">Investment Planning</a></h3>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="text-left comments">
                                        <i class="fa fa-user"></i>
                                        <a href="https://themewidget.com/corporateagency.demo/index.php/author/admin/">
                                            admin </a>
                                    </div>
                                    <div class="date">
                                        <span><i class="fa fa-calendar"></i> December 25, 2018</span>
                                    </div>
                                </div>
                            </div>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when [&hellip;]</p>
                            <a href="https://themewidget.com/corporateagency.demo/index.php/2018/12/25/investment-planning/" class="btn btn-primary">Read More <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section9" class="section-margine section-9-background">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-lg-2">
                        <img width="130" height="78" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6.png" class="img-responsive wow tada wp-post-image" alt="1" />
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <img width="130" height="78" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/6.png" class="img-responsive wow tada wp-post-image" alt="2" />
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <img width="130" height="78" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/3.png" class="img-responsive wow tada wp-post-image" alt="3" />
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <img width="130" height="78" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/4.png" class="img-responsive wow tada wp-post-image" alt="4" />
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <img width="130" height="78" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/2.png" class="img-responsive wow tada wp-post-image" alt="5" />
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <img width="130" height="78" src="https://themewidget.com/corporateagency.demo/wp-content/uploads/2018/12/1.png" class="img-responsive wow tada wp-post-image" alt="6" />
                    </div>
                </div>
            </div>
        </section>
        <section id="footer-top" class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div id="text-2" class="footer-top-box widget widget_text">
                            <h4 class="widget-title">About Us</h4>
                            <div class="textwidget">
                                <div class="footer-top-box">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard dummy text</p>
                                </div>
                                <h4 class="widget-title">Office Hour</h4>
                                <div class="textwidget">
                                    <p><b>Mon-Fri :</b> 09am to 06pm<br />
                                        <b>Sun-Sat :</b> Special Appointment
                                    </p>
                                </div>
                                <div class="footer-top-box"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div id="mc4wp_form_widget-2" class="footer-top-box widget widget_mc4wp_form_widget">
                            <h4 class="widget-title">Subscribe</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div id="text-3" class="footer-top-box widget widget_text">
                            <h4 class="widget-title">Contact us</h4>
                            <div class="textwidget">
                                <p><b>Location : 42/2</b> Queens, NewYork<br />
                                    <b>Phone: </b>+ 1 (1800) 459 123 7<br />
                                    <b>Mail: </b>info@support.com
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="footer-bottom" class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <div class="copyright">
                            <a href="https://wordpress.org/">
                                Proudly powered by WordPress </a>
                            <span class="sep"> | </span>
                            Theme: Corporate Agency by <a href="https://themewidget.com/">Theme Widget</a>.
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <ul class="list-inline social-buttons">
                            <li>
                                <a href="https://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com" target="_blank"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="https://pinterest.com" target="_blank"><i class="fa fa-pinterest"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </div><!-- #page -->

    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/themewidget.com\/corporateagency.demo\/index.php\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/scripts.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/js.cookie.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/corporateagency.demo\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/corporateagency.demo\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/woocommerce.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/corporateagency.demo\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/corporateagency.demo\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_ea602aff36df8d4918c819b540306164",
            "fragment_name": "wc_fragments_ea602aff36df8d4918c819b540306164"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/cart-fragments.min.js'></script>
    <script type='text/javascript'>
        jQuery('body').bind('wc_fragments_refreshed', function() {
            jQuery('body').trigger('jetpack-lazy-images-load');
        });
    </script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/skip-link-focus-fix.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/bootstrap.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/jquery.isotope.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/jquery.magnific-popup.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/owl.carousel.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/wow.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/main.js'></script>
    <script type='text/javascript' src='wp-content/themes/corporate-agency/assets/js/customizer/wp-embed.min.js'></script>

</body>

</html>